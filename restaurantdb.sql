-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 31 Janvier 2016 à 01:45
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `restaurantdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL,
  `total` float NOT NULL,
  `table` int(11) NOT NULL,
  `serveur` varchar(100) NOT NULL,
  `etat` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`id`, `total`, `table`, `serveur`, `etat`) VALUES
(1, 330, 12, 'tarraq', 'nouvelle'),
(2, 160, 10, 'tarraq', 'servie'),
(3, 300, 8, 'tarraq', 'en cours'),
(4, 350, 4, 'lemkhantar', 'nouvelle'),
(5, 270, 7, 'lemkhantar', 'en cours');

-- --------------------------------------------------------

--
-- Structure de la table `commande_produit`
--

CREATE TABLE IF NOT EXISTS `commande_produit` (
  `idCommande` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`idCommande`,`idProduit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `commande_produit`
--

INSERT INTO `commande_produit` (`idCommande`, `idProduit`, `quantite`, `total`) VALUES
(1, 2, 1, 20),
(1, 3, 1, 10),
(1, 4, 1, 20),
(1, 7, 2, 240),
(1, 10, 1, 40),
(2, 3, 2, 20),
(2, 4, 1, 20),
(2, 8, 2, 120),
(3, 1, 1, 70),
(3, 3, 2, 20),
(3, 7, 1, 120),
(3, 9, 1, 30),
(3, 10, 1, 40),
(3, 11, 2, 20),
(4, 1, 4, 280),
(4, 3, 2, 20),
(4, 4, 1, 20),
(4, 9, 1, 30),
(5, 1, 1, 70),
(5, 2, 2, 40),
(5, 3, 1, 10),
(5, 8, 2, 120),
(5, 9, 1, 30);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(100) NOT NULL,
  `prix` float NOT NULL,
  `categorie` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `intitule`, `prix`, `categorie`) VALUES
(1, 'Tajine de poisson', 70, 'plat'),
(2, 'Jus d''orange', 20, 'boisson'),
(3, 'Coca-cola', 10, 'boisson'),
(4, 'Glace au chocolat', 20, 'dessert'),
(7, 'Pastilla de poisson', 120, 'plat'),
(8, 'Poisson grillé', 60, 'plat'),
(9, 'Salade de fruits', 30, 'dessert'),
(10, 'Gâteau au caramel', 40, 'dessert'),
(11, 'Eau minerale', 10, 'boisson');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `statut` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `login`, `password`, `statut`) VALUES
(1, 'lemkhantar', 'chakib', 'lemkhantar1', 'lemkhantar1', 'serveur'),
(2, 'nabil', 'oumayma', 'oumayma1', 'oumayma1', 'cuisinier'),
(3, 'tarraq', 'mohammed', 'tarraq1', 'tarraq1', 'serveur');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
