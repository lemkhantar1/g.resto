﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GestionRestaurant.Constantes;
using MySql.Data.MySqlClient;

namespace GestionRestaurant.Models
{
    public class BaseConnection
    {
        private static MySqlConnection connection = null;

        static string serveur = ValeursBD.serveur;
        static string DataBase = ValeursBD.baseDonnees;
        static string login = ValeursBD.login;
        static string password = ValeursBD.password;

        static string connexionString = @"Data Source=" + serveur + "; Database=" + DataBase + ";  User ID=" + login + " ; Password=" + password;

        private BaseConnection() {}

        public static MySqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    MySqlConnection connection = new MySqlConnection(connexionString);
                    connection.Open();
                    return connection;
                }
                return connection;
            }
        }
    }
}