﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionRestaurant.Models
{
    public class Produit
    {
        public int id { get; set; }
        public int idServeur { get; set; }
        public int idCommande { get; set; }
        public string intitule { get; set; }
        public float prix { get; set; }
        public string categorie { get; set; }
        public int quantite { get; set; }
        public float prixU { get; set; }

        public int EstDans(List<Produit> list)
        {
            for(int i=0; i<list.Count; i++)
            {
                if (list.ElementAt(i).id == id)
                    return i;
            }
            return -1;
        }
    }
}