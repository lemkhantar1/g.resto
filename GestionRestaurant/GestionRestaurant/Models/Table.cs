﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionRestaurant.Models
{
    public class Table
    {
        public int id { get; set; }
        public float total { get; set; }
        public int nbCommandes { get; set; }
    }
}