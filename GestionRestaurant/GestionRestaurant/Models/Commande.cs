﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionRestaurant.Models
{
    public class Commande
    {
        public int id { get; set; }
        public float total { get; set; }
        public string etat { get; set; }
        public int table { get; set; }
        public string serveur { get; set; }
    }
}