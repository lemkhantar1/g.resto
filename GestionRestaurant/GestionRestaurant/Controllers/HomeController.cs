﻿using GestionRestaurant.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionRestaurant.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session.Clear();
            Session.Abandon();
            return View();
        }
        [HttpPost]
        public ActionResult Authentification()
        {
            string login = Request.Params["login"];
            string password = Request.Params["password"];

            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM utilisateur WHERE login ='"+login+"' AND password = '"+password+"'";
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if(reader.HasRows)
            {
                Session["nom"] = reader["nom"].ToString();
                Session["prenom"] = reader["prenom"].ToString();
                Session["statut"] = reader["statut"].ToString();
                if (reader["statut"].ToString().Equals("cuisinier"))
                {
                    return RedirectToAction("Cuisinier", "Home");
                }
                else if (reader["statut"].ToString().Equals("serveur"))
                {
                    return RedirectToAction("Accueil", "Home");
                }
            }
            
            return RedirectToAction("Index", "Home");

        }
        public ActionResult ChangerEtatCuisinier(int? id, string etat)
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            if (etat.Equals("en cours"))
            {
                cmd.CommandText = "UPDATE commande SET etat = 'servie' WHERE id = " + id;
            }
            else
            {
                cmd.CommandText = "UPDATE commande SET etat = 'en cours' WHERE id = " + id;
            }
            cmd.ExecuteNonQuery();
            return View("Cuisinier");
        }
        public ActionResult ChangerEtatServeur(int? id)
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE commande SET etat = 'payee' WHERE id = " + id;
            cmd.ExecuteNonQuery();
            return RedirectToAction("Commandes", "Home");
        }
        public ActionResult SupprimerCommande(int? id)
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT etat FROM commande WHERE id = " + id;
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            if(reader["etat"].ToString().Equals("nouvelle"))
            {
                reader.Close();
                MySqlCommand cmd2 = conn.CreateCommand();
                cmd2.CommandText = "DELETE FROM commande WHERE id = "+id;
                cmd2.ExecuteNonQuery();
                cmd2.CommandText = "DELETE FROM commande_produit WHERE idCommande = " + id;
                cmd2.ExecuteNonQuery();
            }
            return RedirectToAction("Commandes", "Home");
        }
        public ActionResult ModifierCommande(int? id)
        {
            float total = 0;
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM commande WHERE id = " + id;
            MySqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            int table = int.Parse(reader["table"].ToString());
            if (reader["etat"].ToString().Equals("nouvelle"))
            {
                reader.Close();
                MySqlCommand cmd2 = conn.CreateCommand();
                cmd2.CommandText = "SELECT * FROM commande_produit WHERE idCommande = " + id;
                reader = cmd2.ExecuteReader();
                List<Produit> listeCmd = new List<Produit>();
                Produit produit;
                while(reader.Read())
                {
                    produit = new Produit();
                    produit.id = int.Parse(reader["idProduit"].ToString());
                    produit.quantite = int.Parse(reader["quantite"].ToString());
                    produit.prix = float.Parse(reader["total"].ToString());
                    total += produit.prix;
                    listeCmd.Add(produit);
                }
                reader.Close();
                foreach(Produit pro in listeCmd)
                {
                    cmd2.CommandText = "SELECT * FROM produit WHERE id =" + pro.id;
                    reader = cmd2.ExecuteReader();
                    reader.Read();
                    pro.intitule = reader["intitule"].ToString();
                    pro.prixU = float.Parse(reader["prix"].ToString());
                    reader.Close();
                    cmd2.CommandText = "DELETE FROM commande_produit WHERE idCommande = " + id;
                    cmd2.ExecuteNonQuery();
                    cmd2.CommandText = "DELETE FROM commande WHERE id = " + id;
                    cmd2.ExecuteNonQuery();
                }
                Session["cmd"] = listeCmd;
                Session["table"] = table;
                Session["total"] = total;
                return View("Commande");
            }
            return RedirectToAction("Commandes", "Home");
        }
        public ActionResult Accueil()
        {
            if (Session["statut"] != null)
            {
                if (Session["statut"].ToString().Equals("serveur"))
                    return View();
                else
                    return RedirectToAction("Cuisinier");
            }
            return RedirectToAction("Index");
        }
        public ActionResult Tables()
        {
            if (Session["statut"] != null)
            {
                if (Session["statut"].ToString().Equals("serveur"))
                    return View();
                else
                    return RedirectToAction("Cuisinier");
            }
            return RedirectToAction("Index");
        }
        public ActionResult Commandes()
        {
            if (Session["statut"] != null)
            {
                if (Session["statut"].ToString().Equals("serveur"))
                {
                    MySqlConnection conn = BaseConnection.Connection;
                    MySqlCommand cmd = conn.CreateCommand();
                    string nomServeur = Session["nom"].ToString();
                    cmd.CommandText = "SELECT * FROM commande WHERE serveur = '" + nomServeur + "' AND etat <> 'payee'";
                    MySqlDataReader reader = cmd.ExecuteReader();
                    List<Commande> liste = new List<Commande>();
                    while (reader.Read())
                    {
                        Commande commande = new Commande();
                        commande.table = int.Parse(reader["table"].ToString());
                        commande.etat = reader["etat"].ToString();
                        commande.total = float.Parse(reader["total"].ToString());
                        commande.id = int.Parse(reader["id"].ToString());
                        liste.Add(commande);
                    }
                    ViewBag.liste = liste;
                    conn.Close();
                    return View();
                }
                else
                    return RedirectToAction("Cuisinier");
            }
            else
                return RedirectToAction("Index");
           
        }
        public ActionResult Commande()
        {
            if(Request.Params["table"] != null)
            {
                Session["table"] = Request.Params["table"];
                return View();
            }
            else if(Session["table"]!=null)
            {
                return View();
            }
            else
            {
                if (Session["statut"] != null)
                {
                    if (Session["statut"].ToString().Equals("serveur"))
                        return RedirectToAction("Tables");
                    else
                        return RedirectToAction("Cuisinier");
                }
                return RedirectToAction("Index");
            }
            
        }
        public ActionResult Cuisinier()
        {
            if(Session["statut"] != null)
            {
                if (Session["statut"].ToString().Equals("cuisinier"))
                    return View();
                else
                    return RedirectToAction("Accueil");
            }
            return RedirectToAction("Index");
            
        }
        public ActionResult Valider()
        {
            int idCommande;
            string nomServeur = Session["nom"].ToString();
            string etat = "nouvelle";
            int table = int.Parse(Session["tableCourante"].ToString());
            float total = 0;
            List<Produit> cmd = (List<Produit>)Session["cmd"];

            using (MySqlConnection conn = BaseConnection.Connection)
            {
                MySqlCommand req = conn.CreateCommand();
                req.CommandText = "SELECT COUNT(id) AS n FROM commande";
                MySqlDataReader reader = req.ExecuteReader();
                reader.Read();
                if (reader["n"].ToString().Equals("0"))
                {
                    reader.Close();
                    idCommande = 1;
                }
                else
                {
                    reader.Close();
                    req.CommandText = "SELECT MAX(id) AS max FROM commande";
                    reader = req.ExecuteReader();
                    reader.Read();
                    idCommande = int.Parse(reader["max"].ToString()) + 1;
                    reader.Close();
                }






                for (int i = 0; i < cmd.Count; i++)
                {
                    req.CommandText = "INSERT INTO `commande_produit` (`idCommande`, `idProduit`, `quantite`, `total`) VALUES (" + idCommande + "," + cmd.ElementAt(i).id + "," + cmd.ElementAt(i).quantite + "," + cmd.ElementAt(i).prix + ")";
                    req.ExecuteNonQuery();
                    total += cmd.ElementAt(i).prix;
                }
            }
            using (MySqlConnection conn = BaseConnection.Connection)
            {
                MySqlCommand req = conn.CreateCommand();
                req.CommandText = "INSERT INTO `commande` (`id`, `total`, `table`, `serveur`, `etat`) VALUES (" + idCommande + "," + total + "," + table + ",'" + nomServeur + "','" + etat + "')";
                req.ExecuteNonQuery();

            }


            Session["cmd"] = null;
            Session["total"] = null;
            Session["table"] = null;
            return RedirectToAction("Commandes", "Home");
        }
        public ActionResult Bilan()
        {
            if (Session["statut"] != null)
            {
                if (Session["statut"].ToString().Equals("serveur"))
                {
                    float total;
                    float totalGeneral = 0;
                    int nbCommandes;
                    List<Table> tables;
                    using (MySqlConnection conn = BaseConnection.Connection)
                    {
                        tables = new List<Table>();
                        MySqlCommand cmd = conn.CreateCommand();
                        string nom = Session["nom"].ToString();
                        cmd.CommandText = "SELECT DISTINCT `table`,`serveur` FROM `commande`";
                        MySqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            if (!reader["serveur"].ToString().Equals(nom)) continue;
                            Table table = new Table();
                            table.id = int.Parse(reader["table"].ToString());
                            tables.Add(table);
                        }
                        reader.Close();
                        foreach (Table table in tables)
                        {
                            total = 0;
                            nbCommandes = 0;
                            cmd.CommandText = "SELECT `total` FROM `commande` WHERE `table`=" + table.id;
                            reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                total += float.Parse(reader["total"].ToString());
                                nbCommandes++;
                            }
                            table.total = total;
                            totalGeneral += total;
                            table.nbCommandes = nbCommandes;
                            reader.Close();
                        }
                    }
                    Session["totalGeneral"] = totalGeneral;
                    return View("Bilan", tables);
                }
                else
                    return RedirectToAction("Cuisinier");
            }
            else
                return RedirectToAction("Index");
            
        }
        public ActionResult Deconnexion()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index");
        }
        public PartialViewResult MenuBoisson()
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM produit WHERE categorie = 'boisson'";
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Produit> liste = new List<Produit>();
            while (reader.Read())
            {
                Produit produit = new Produit();
                produit.intitule = reader["intitule"].ToString();
                produit.prix = float.Parse(reader["prix"].ToString());
                produit.id = int.Parse(reader["id"].ToString());
                liste.Add(produit);
            }
            return PartialView("_MenuBoisson", liste);
        }
        public PartialViewResult MenuPlat()
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM produit WHERE categorie = 'plat'";
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Produit> liste = new List<Produit>();
            while (reader.Read())
            {
                Produit produit = new Produit();
                produit.intitule = reader["intitule"].ToString();
                produit.prix = float.Parse(reader["prix"].ToString());
                produit.id = int.Parse(reader["id"].ToString());
                liste.Add(produit);
            }
            return PartialView("_MenuPlat", liste);
        }
        public PartialViewResult MenuDessert()
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM produit WHERE categorie = 'dessert'";
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Produit> liste = new List<Produit>();
            while (reader.Read())
            {
                Produit produit = new Produit();
                produit.intitule = reader["intitule"].ToString();
                produit.prix = float.Parse(reader["prix"].ToString());
                produit.id = int.Parse(reader["id"].ToString());
                liste.Add(produit);
            }
            return PartialView("_MenuDessert", liste);
        }
        public PartialViewResult CommandesCuisinier()
        {
            MySqlConnection conn = BaseConnection.Connection;
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM commande WHERE etat <> 'servie' AND etat <> 'payee'";
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Commande> liste = new List<Commande>();
            while (reader.Read())
            {
                Commande commande = new Commande();
                commande.id = int.Parse(reader["id"].ToString());
                commande.etat = reader["etat"].ToString();
                commande.table = int.Parse(reader["table"].ToString());
                commande.serveur = reader["serveur"].ToString();
                liste.Add(commande);
            }
            return PartialView("_CommandesCuisinier", liste);
        }
        public PartialViewResult CommandeCuisinier(int id)
        {
            List<int> produits = new List<int>();
            List<int> quantites = new List<int>();
            using (MySqlConnection conn = BaseConnection.Connection)
            {
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM commande_produit WHERE idCommande=" + id;
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    
                    while (reader.Read())
                    {
                        produits.Add(int.Parse(reader["idProduit"].ToString()));
                        quantites.Add(int.Parse(reader["quantite"].ToString()));
                    }
                }
                List<Produit> listeCommandes = new List<Produit>();
                for (int i = 0; i < produits.Count; i++)
                {
                    Produit el = new Produit();
                    el.quantite = quantites.ElementAt(i);
                    int idProduit = produits.ElementAt(i);
                    cmd.CommandText = "SELECT intitule FROM produit WHERE id=" + idProduit;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        el.intitule = reader["intitule"].ToString(); 
                    }
                    listeCommandes.Add(el);
                }
                Session["id"] = id;
                return PartialView("_CommandeCuisinier", listeCommandes);
            }    
        }
        public PartialViewResult Ajouter(int id)
        {
            List<Produit> cmd;
            float total;
            if (Session["cmd"] == null)
            {
                cmd = new List<Produit>();
            }
            else
            {
                cmd = (List<Produit>)Session["cmd"];
            }
            if (Session["total"] == null)
            {
                total = 0;
            }
            else
            {
                total = float.Parse(Session["total"].ToString());
            }
            using (MySqlConnection conn = BaseConnection.Connection)
            {
                MySqlCommand req = conn.CreateCommand();
                req.CommandText = "SELECT * FROM produit WHERE id=" + id;
                MySqlDataReader reader = req.ExecuteReader();
                reader.Read();
                Produit produit = new Produit();
                produit.intitule = reader["intitule"].ToString();
                produit.quantite = 1;
                produit.prix = float.Parse(reader["prix"].ToString());
                produit.prixU = float.Parse(reader["prix"].ToString());
                total += produit.prixU;
                produit.id = int.Parse(reader["id"].ToString());
                if(produit.EstDans(cmd) == -1)
                {
                    cmd.Add(produit);
                }
                else
                {
                    cmd.ElementAt(produit.EstDans(cmd)).quantite += 1;
                    cmd.ElementAt(produit.EstDans(cmd)).prix += produit.prixU;
                }
                Session["cmd"] = cmd;
                Session["total"] = total;
            }
            return PartialView("_DetailsCommandes",cmd);
        }
        public PartialViewResult Supprimer(int id)
        {
            List<Produit> cmd = (List<Produit>)Session["cmd"];
            float total = float.Parse(Session["total"].ToString());
            Produit produit = new Produit();
            produit.id = id;
            int indice = produit.EstDans(cmd);
            cmd.ElementAt(indice).quantite -= 1;
            cmd.ElementAt(indice).prix -= cmd.ElementAt(indice).prixU;
            total -= cmd.ElementAt(indice).prixU;
            if (cmd.ElementAt(indice).quantite == 0)
            {
                cmd.RemoveAt(indice);
            }
            if(cmd.Count == 0)
            {
                Session["cmd"] = null;
                Session["total"] = null;
            }
            else
            {
                Session["cmd"] = cmd;
                Session["total"] = total;
            } 
            return PartialView("_DetailsCommandes", cmd);
        }
        public PartialViewResult SupprimerTout(int id)
        {
            List<Produit> cmd = (List<Produit>)Session["cmd"];
            float total = float.Parse(Session["total"].ToString());
            Produit produit = new Produit();
            produit.id = id;
            int indice = produit.EstDans(cmd);
            total -= cmd.ElementAt(indice).prix;
            cmd.RemoveAt(indice);
           
            if (cmd.Count == 0)
            {
                Session["cmd"] = null;
                Session["total"] = null;
            }
            else
            {
                Session["cmd"] = cmd;
                Session["total"] = total;
            }
            return PartialView("_DetailsCommandes", cmd);
        }

    }
}