﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionRestaurant.Constantes
{
    public static class ValeursBD
    {
        public static string serveur = "localhost";
        public static string baseDonnees = "restaurantdb";
        public static string login = "root";
        public static string password = "";
        public static string port = "800";
    }
}