Projet réalisé par : 

    * Lemkhantar chakib 
    * Tarraq Mohamed
    * Nabil Oumayma
    * Housni Samia
    * Elfoudani Naima
    * Bendaoudi Imane.


Étapes pour faire fonctionner l'application : 

    * Etape 1 : Créez une base de données MySql avec le nom :  restaurantdb;
    * Etape 2 : Importer le fichier restaurantdb.sql dans la base de données déjà créée;
    * Etape 3 : Dans le dossier " ~/Constantes " Vous trouverez toutes les constantes requises pour la connexion a la base de données, configurez en fonction de votre SGBD.
    * Etape 4 : RUN

NB : pour se connecter, il faut consulter les login dans la base de données, plus spécifiquement dans la table utilisateur.